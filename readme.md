# Command line instructions | Petunjuk Installasi di Terminal / CMD

## Git global setup | Pengaturan Global
Make sure you have installed GIT before, otherwise you can download it first from the following link (https://git-scm.com/downloads)

```
install git before 
git config --global user.name "your_username" 
git config --global user.email "your_email" 
```
## Install virtual server
This application requires a python server to run it. here we use virtualenv, please learn how to use it.

### How toh install virtualenv
```
sudo apt-get install python3-pip
sudo pip3 install virtualenv
```
in windows follow this link(http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/)


## Copy from this project
```
1. pen Terminal in linux / CMD in Windows

2. cd /your_directory/
3. git clone https://gitlab.com/rofiareiv/quick-count.git
4. cd /quick-count/backend/
5. run virtualenv env (assuming you already understand how to use virtualenv)
6. run source env/bin/activate
7. cd /src/
8. pip install -r requirements.text

run this script tu run the project:

9. python manage.py runserver

or skip step 5

10. run source env/bin/activate

and run all step after step 5.
```
