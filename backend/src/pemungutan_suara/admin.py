# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Partai, Pemilu, Kandidat, Tps, Perolehan

class PartaiAdmin (admin.ModelAdmin):
    list_display = ['nama', 'deskripsi']
    search_fields = ['nama', 'deskripsi']
    list_per_page = 25

admin.site.register(Partai, PartaiAdmin)

class PemiluAdmin (admin.ModelAdmin):
    list_display = ['jenis_pemilihan', 'tahun', 'status']
    search_fields = ['jenis_pemilihan', 'tahun', 'status']
    list_per_page = 25

admin.site.register(Pemilu, PemiluAdmin)

class KandidatAdmin (admin.ModelAdmin):
    list_display = ['nama', 'partai', 'no_urut']
    search_fields = ['nama', 'partai', 'no_urut']
    list_per_page = 25

admin.site.register(Kandidat, KandidatAdmin)

class TpsAdmin (admin.ModelAdmin):
    list_display = ['kode_tps', 'nama_tps', 'hak_suara']
    search_fields = ['kode_tps', 'nama_tps', 'hak_suara']
    list_per_page = 25

admin.site.register(Tps, TpsAdmin)

class PerolehanAdmin (admin.ModelAdmin):
    list_display = ['pemilu', 'kandidat', 'tps', 'jumlah']
    search_fields = ['pemilu', 'kandidat', 'tps', 'jumlah']
    list_per_page = 25
    ordering = ('kandidat','jumlah')
    raw_id_fields = ('pemilu', 'kandidat', 'tps')

admin.site.register(Perolehan, PerolehanAdmin)
