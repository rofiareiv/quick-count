# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from uuid import uuid4

from django.db import models

# Create your function here.
def get_file_image(instance, filename):
  ext = filename.split('.')[-1]
  filename = "%s.%s" % (str(uuid4()), ext)
  return os.path.join('images', filename)
# Create your models here.
class Partai (models.Model):
    nama = models.CharField(max_length=200)
    deskripsi = models.TextField()
    photo = models.ImageField(upload_to=get_file_image, height_field=None, blank=True, width_field=None, max_length=None)

    def __str__(self):
        return self.nama
    
    class Meta:
      verbose_name_plural = 'Partai'

class Pemilu (models.Model):
    JENIS_PEMILIHAN = (
        ('Pilpres', 'Pemilihan Presiden'),
        ('Pilgub', 'Pemilihan Gubernur'),
        ('Pilbub', 'Pemilihan Bupati'),
        ('Serentak', 'Pemilihan Serentak')
    )
    STATUS = (
        ('Y', 'Aktif'),
        ('N', 'Tidak Aktif')
    )

    jenis_pemilihan = models.CharField(max_length=50, choices=JENIS_PEMILIHAN)
    tahun = models.CharField(max_length=50)
    status = models.CharField(max_length=10, choices=STATUS)

    def __str__(self):
        return self.jenis_pemilihan
    
    class Meta:
      verbose_name_plural = 'Pemilu'

class Kandidat (models.Model):
    POSISI_KANDIDAT = (
        ('Utama', 'Utama'),
        ('Wakil', 'Wakil')
    )
    nama = models.CharField(max_length=100)
    partai = models.ForeignKey("Partai", on_delete=models.CASCADE)
    photo = models.ImageField(upload_to=get_file_image, height_field=None, width_field=None, max_length=None)
    no_urut = models.CharField(max_length=10)
    pemilu = models.ForeignKey("Pemilu", on_delete=models.CASCADE)
    posisi = models.CharField(max_length=100, choices=POSISI_KANDIDAT)

    def __str__(self):
        return self.nama
    
    class Meta:
      verbose_name_plural = 'Kandidat Pemilu'

class Tps (models.Model):
    kode_tps = models.CharField(max_length=50)
    nama_tps = models.CharField(max_length=50)
    kabupaten = models.ForeignKey("wilayah.Provinsi", on_delete=models.CASCADE)
    kecamatan = models.ForeignKey("wilayah.Kecamatan", on_delete=models.CASCADE)
    desa = models.ForeignKey("wilayah.Desa", on_delete=models.CASCADE)
    hak_suara = models.CharField(max_length=50)

    def __str__(self):
        return self.kode_tps
    class Meta:
      verbose_name_plural = 'TPS'

class Perolehan (models.Model):
    pemilu = models.ForeignKey("Pemilu", on_delete=models.CASCADE, limit_choices_to={'status': 'Y'})
    kandidat = models.ForeignKey("Kandidat", on_delete=models.CASCADE)
    tps = models.ForeignKey("Tps", on_delete=models.CASCADE)
    jumlah = models.CharField(max_length=100)

    def __str__(self):
        return self.jumlah
    
    class Meta:
      verbose_name_plural = 'Perolehan Suara'
