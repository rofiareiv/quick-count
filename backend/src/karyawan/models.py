# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Divisi (models.Model):
    nama = models.CharField(max_length=100)
    keterangan = models.TextField(blank=True)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Divisi'

class Jabatan (models.Model):
    nama = models.CharField(max_length=100)
    keterangan = models.TextField(blank=True)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Jabatan'

class Karyawan (models.Model):
    JENIS_KELAMIN = (
        ('pria', 'Pria'),
        ('wanita', 'Wanita')
    )

    JENIS_KARYAWAN = (
        ('kontrak', 'Kontrak'),
        ('tetap', 'Tetap')
    )

    nama = models.CharField(max_length=100)
    alamat = models.TextField(blank=True)
    jenis_kelamin = models.CharField(max_length=50, choices=JENIS_KELAMIN)
    jenis_karyawan = models.CharField(max_length=50, choices=JENIS_KARYAWAN)
    no_telepon = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=254)
    divisi = models.ForeignKey("Divisi", on_delete=models.CASCADE)
    jabatan = models.ForeignKey("Jabatan", on_delete=models.CASCADE)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Karyawan'

class Akun (models.Model):
    JENIS_AKUN = (
        ('karyawan', 'Karyawan'),
        ('admin', 'Administrator')
    )

    akun = models.ForeignKey(User, on_delete=models.CASCADE)
    karyawan = models.ForeignKey(Karyawan, on_delete=models.CASCADE)
    jenis_akun = models.CharField(max_length=50, choices=JENIS_AKUN)

    def __str__(self):
        return self.karyawan.nama
    
    class Meta:
        verbose_name_plural = 'Akun'
