# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import *

class NegaraAdmin (admin.ModelAdmin):
    list_display = ['kode', 'nama']
    search_fields = ['kode', 'nama']
    list_per_page = 25

admin.site.register(Negara, NegaraAdmin)

class ProvinsiAdmin (admin.ModelAdmin):
    list_display = ['kode', 'nama',]
    search_fields = ['kode', 'nama']
    list_per_page = 25

admin.site.register(Provinsi, ProvinsiAdmin)

class KabupatenAdmin (admin.ModelAdmin):
    list_display = ['kode', 'nama', 'provinsi']
    search_fields = ['kode', 'nama', 'provinsi']
    list_per_page = 25

admin.site.register(Kabupaten, KabupatenAdmin)

class KecamatanAdmin (admin.ModelAdmin):
    list_display = ['kode', 'nama', 'provinsi', 'kabupaten']
    search_fields = ['kode', 'nama', 'provinsi', 'kabupaten']
    list_per_page = 25

admin.site.register(Kecamatan, KecamatanAdmin)

class DesaAdmin (admin.ModelAdmin):
    list_display = ['kode', 'nama', 'provinsi', 'kabupaten', 'kecamatan']
    search_fields = ['kode', 'nama', 'provinsi', 'kabupaten', 'kecamatan']
    list_per_page = 25

admin.site.register(Desa, DesaAdmin)
