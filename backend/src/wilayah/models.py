# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
# Create your models here.
class Negara (models.Model):
    kode = models.CharField(max_length=50)
    nama = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Negara'
    
class Provinsi (models.Model):
    kode = models.CharField(max_length=50)
    negara = models.ForeignKey("Negara", on_delete=models.CASCADE)
    nama = models.CharField(max_length=100)    

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Provinsi'

class Kabupaten (models.Model):
    kode = models.CharField(max_length=50)
    provinsi = models.ForeignKey("Provinsi", on_delete=models.CASCADE)
    nama = models.CharField(max_length=100)    

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Kabupaten'

class Kecamatan (models.Model):
    kode = models.CharField(max_length=50)
    provinsi = models.ForeignKey("Provinsi", on_delete=models.CASCADE)
    kabupaten = models.ForeignKey("Kabupaten", on_delete=models.CASCADE)
    nama = models.CharField(max_length=100)    

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name = 'Kecamatan'
        verbose_name_plural = 'Kecamatan'

class Desa (models.Model):
    kode = models.CharField(max_length=50)
    provinsi = models.ForeignKey("Provinsi", on_delete=models.CASCADE)
    kabupaten = models.ForeignKey("Kabupaten", on_delete=models.CASCADE)
    kecamatan = models.ForeignKey("Kecamatan", on_delete=models.CASCADE)
    nama = models.CharField(max_length=100)    

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Desa'
        