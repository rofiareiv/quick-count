# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, render_to_response
# from django.template.context import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.conf import settings

# from django.contrib.auth.views import logout as original_logout
# from django.contrib.auth.views import login as original_login
from django.contrib import messages

from karyawan.models import Akun, Karyawan
# Create your views here.
def login_view(request):
  if request.POST:
    user = authenticate(username=request.POST['username'], password=request.POST['password'])
    if user is not None:
      if user.is_active:
        try:
          akun = Akun.objects.get(akun=user.id)
          login(request, user)

          request.session['karyawan_id'] = akun.karyawan.id
          request.session['jenis_akun'] = akun.jenis_akun
          request.session['username'] = request.POST['username']
        except:
          messages.add_message(request, messages.INFO, 'akun belum terhubung dengan karyawan')
        return redirect('/')
      else:
        messages.add_message(request, messages.INFO, 'username atau password salah')
  return render(request, 'login.html', {})

def logout_view(request):
  logout(request)
  return redirect('/login/')

@login_required(login_url=settings.LOGIN_URL)
def dashbord(request):
  if request.session.has_key('karyawan_id'):
    karyawan = Karyawan.objects.get(id=request.session['karyawan_id'])
    return render(request, 'dashbord.html', {})
  else:
    return render(request, 'login.html', {})