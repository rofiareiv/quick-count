from django.conf.urls import url
from django.contrib import admin

from homepage import views as homepage_views
from karyawan import views as karyawan_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^$', karyawan_views.profil),
    url(r'^$', homepage_views.dashbord),
    url(r'^login/', homepage_views.login_view),
    url(r'^logout/', homepage_views.logout_view),
    url(r'^profil/', karyawan_views.profil),
]

admin.site.site_header = 'QUICO ADMIN'
admin.site.site_title = 'QUICO'
admin.site.index_title = 'Welcome To QUICO System'
